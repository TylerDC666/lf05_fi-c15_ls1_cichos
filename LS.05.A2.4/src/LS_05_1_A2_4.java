import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double anzahlTickets;
       
       //Eingabe:
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Wie viele Tickets wollen sie?");
       anzahlTickets = tastatur.nextDouble();
       
       zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;


       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   
    	   System.out.print("Noch zu zahlen: ");
    	   System.out.printf("%.2f\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) 
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5)
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) 
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) 
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
 	          
 	          tastatur.close();
           }
       }

       System.out.println("\nBitte den Fahrschein\n"+
                          "vor Fahrtantritt entwerten!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}