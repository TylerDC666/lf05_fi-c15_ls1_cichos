import java.util.Scanner;

class Fahrkartenautomat
{
	  public static void main(String[] args) {
		  fahrkartenbestellungErfassen();
	  }
	  
	 public static void warte(int millisekunde)
	 {
		  try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
		}
	  }
	 
    public static void fahrkartenbestellungErfassen()
    { 
    	 Scanner tastatur = new Scanner(System.in);
    	
    	
    	double zuZahlenderBetrag; 
    	short ticketMenge;
       System.out.print("Ticketpreis (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble(); 

       System.out.print("Anzahl der Tickets: ");
       ticketMenge = tastatur.nextShort();
       
       zuZahlenderBetrag = ticketMenge * zuZahlenderBetrag;
       
       fahrkartenBezahlen(zuZahlenderBetrag);
    }
       
    public static void fahrkartenBezahlen(double zuZahlenderBetrag) {
    	 Scanner tastatur = new Scanner(System.in);
    	 double eingezahlterGesamtbetrag;
    	  double eingeworfeneM�nze;
    	 
    	 eingezahlterGesamtbetrag = 0.0;
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   
     	   System.out.print("Noch zu zahlen: ");
     	   System.out.printf("%.2f\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }
    
       public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag )
       {
    	    Scanner tastatur = new Scanner(System.in);
  
       System.out.println("\nFahrschein wird ausgegeben");
       
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          warte(250);
       }
       System.out.println("\n\n");
       rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
       }
    
    public static void rueckgeldAusgeben( double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	 double r�ckgabebetrag;   

       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.print("Der R�ckgabebetrag in H�he von ");
    	   System.out.printf("%.2f", r�ckgabebetrag);
    	   System.out.println(" EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {

        	  muenzeAusgeben(2,"EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	   muenzeAusgeben(1,"EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	   muenzeAusgeben(50,"CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	   muenzeAusgeben(20,"CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	   muenzeAusgeben(10,"CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	   muenzeAusgeben(5,"CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	  System.out.println(betrag + " " + einheit);
    }
}